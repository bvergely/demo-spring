package com.example.yaPerfAdmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class YaPerfAdminApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(YaPerfAdminApplication.class, args);
	}

}
